# Plan de Trabajo - CONICET

Plantilla en LaTeX para la presentación de planes de trabajo a becas doctorado y postdoctorado de acuerdo con el formato requerido por CONICET.

## Características

- Los documentos terminados en LaTeX tiene altos estándares de tipografía.
- Alto control para manejar aspectos geométricos del documentos, esto es, interlineados, ancho/alto de página, sangrías, separación entre párrafos, etc. La regla general es que el documento no supere las 5 hojas de extensión.
- Uso del paquete [``microtype``](https://ctan.org/pkg/microtype?lang=en). *Subliminal refinements towards typographical perfection*. No encontrarán nada que se le parezca en ningún paquete de ofimática.
- Integra algunos fields como author/title como metadata al pdf.
- Implementa cuadro de tareas estipuladas por el plan de trabajo para los planes de doctorado (5 años) y postdoctorado (3 años) en forma trimestral. 
- Implementa referencias en BibTex.
- Utiliza fuente tipo Arial, tamaño 11. Para utilizar la fuente Arial, el documento debe ser compilado con LuaLatex.


## Getting started

Para no sufrir con la compilación a través de LuaLatex y el ``fontspec`` para hacer uso de la fuente ``Arial`` recomiendo utilizar [Overleaf](https://www.overleaf.com/), un editor de LaTeX online. [Aquí](https://www.overleaf.com/read/rmpnyzmmfftb) se encuentra alojado el proyecto.

## How it looks

### Inicio
![screenshot](/uploads/dfe62bf70459d02ff39e7bfa7bc7fb8f/screenshot.png)

### Cuadro de tareas
![screenshoot_2](/uploads/9a4132d1284c0a8875ce233ad38c8d2b/screenshoot_2.png)



